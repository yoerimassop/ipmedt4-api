<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//Routes voor gerechten restaurant
Route::post('/product/create', 'ProductController@store');
Route::patch('/product/update/{id}', 'ProductController@update');
Route::delete('/product/delete/{id}', 'ProductController@destroy');
Route::get('/product/{id}','ProductController@show');
Route::get('/product','ProductController@index');
Route::get('/producten/{restaurant}','ProductController@emailShow');

//Route voor status update van bestelling
Route::patch('bestelling/update/{ordernummer}', 'BestellingController@updateStatusBestelling');

Route::post('restaurant', 'RestaurantController@register');
Route::patch('edituser/{email}','UserController@update');

Route::get('/bezorger/{email}', 'UserController@show');
Route::patch('/bezorger/location/update/{email}/{location}', 'UserController@updateBezorgerLocation');


Route::get('/review','ReviewController@index');
Route::get('/review/{restaurant}','ReviewController@show');
Route::post('/review/storeReview','ReviewController@storeReview');

Route::post('register', 'UserController@register');
Route::post('login', 'UserController@login');
Route::get('profile', 'UserController@getAuthenticatedUser');

Route::post('restaurant', 'RestaurantController@register');

//Route voor restaurants ophalen voor klantbestelling
Route::get('/restaurant/klantbestelling', 'RestaurantController@restaurantKlantbestelling');

//Route voor categorie filtering.
Route::get('/restaurant/{categorie}', 'RestaurantController@categorie');

//Route voor ophalen restaurant card
Route::get('/restaurant/{naam}/info', 'RestaurantController@info');

//Route voor product in bestelling
Route::post('/productinbestelling/create', 'ProductInBestellingController@create');


// Bestelling functies
Route::post('/bestelling/create', 'BestellingController@create');
Route::get('/bestelling/laatsteBestelling', 'BestellingController@laatsteBestelling');
Route::get('/bestelling/{ordernummer}','BestellingController@show');
Route::get('/bestelling/status/{status}','BestellingController@status');

Route::get('/bestelling/bezorger/{status}/{email}','BestellingController@bezorger');
Route::get('/bestelling/bezorger/{status}/{email}/{bezorgdatum}','BestellingController@bezorgdatum');

Route::patch('bestelling/updateStatus/{ordernummer}/{status}/{email}', 'BestellingController@updateStatus');

Route::middleware('auth.api')->get('/user', function(Request $request){
  return $request->user();
});
