<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/user/{email}','UserController@show');
Route::get('/user','UserController@index');

Route::get('/restaurant/{email}','RestaurantController@emailShow');
Route::get('/restaurant/{naam}','RestaurantController@show');
Route::get('/restaurant','RestaurantController@index');

Route::post('bestelling/create', 'BestellingController@store');
Route::get('/bestelling/user', 'BestellingController@user');
Route::get('/bestelling/restaurant/{restaurant}', 'BestellingController@restaurantBestellingen');
Route::get('/bestelling/{ordernummer}','BestellingController@show');
Route::get('/bestelling','BestellingController@index');
Route::get('/bestelling/{ordernummer}/producten','BestellingController@producten');
Route::get('/bestelling/{ordernummer}/adres','BestellingController@adres');



Route::get('/favoriet/{id}','FavorietController@show');
Route::get('/favoriet','FavorietController@index');

Route::get('/review/{restaurant}','ReviewController@show');
Route::get('/review','ReviewController@index');
Route::post('/review/create', 'ReviewController@store');

Route::get('/', function () {
    return view('welcome');
});
