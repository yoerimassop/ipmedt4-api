<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRestaurantTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('restaurant', function (Blueprint $table) {
            $table->timestamps();
            $table->string('naam')->primary();
            $table->string('priveEmail');
            $table->longText('logo');
            $table->string('telefoonnummer');
            $table->string('KVK_nummer');
            $table->integer('rating')->default(5);
            $table->string('BTW_nummer');
            $table->string('prive_nummer');
            $table->string('postcode');
            $table->string('huisnummer');
            $table->string('woonplaats');
            $table->string('straatnaam');
            $table->char('beschrijving', 75);
            $table->string('categorie');
            $table->foreign('priveEmail')->references('email')->on('users')->onUpdate('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::table('restaurant', function (Blueprint $table) {
        $table->dropForeign('restaurant_priveEmail_foreign');
      });

        Schema::dropIfExists('restaurant');
    }
}
