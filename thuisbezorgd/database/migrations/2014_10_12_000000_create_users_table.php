<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->string('naam');
            $table->string('email')->primary()->default('test');
            $table->string('telefoonnummer')->default('0252-233489');
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password');
            $table->string('geslacht')->default('M');
            $table->string('postcode')->default('2215MB');
            $table->string('huisnummer')->default('13');
            $table->string('woonplaats')->default('voorhout');
            $table->string('straatnaam')->default('pieter');
            $table->date('geboortedatum')->default('2001-02-02');
            $table->string('rol')->default('klant');
            $table->rememberToken();
            $table->timestamps();
            $table->string('location')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
