<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBestellingTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bestelling', function (Blueprint $table) {
            $table->id('ordernummer');
            $table->integer('totaalbedrag');
            $table->date('bezorgdatum')->nullable();
            $table->string('bezorgtijd')->nullable();
            $table->string('betaalmethode')->nullable();
            $table->string('email_klant');
            $table->string('email_bezorger')->nullable();
            $table->string('naam_restaurant');
            $table->string('status')->default('geplaatst');
            $table->string('bezorgadres')->nullable();
            //Voor bestelling status update
            $table->dateTimeTz('updated_at')->nullable();
            $table->dateTimeTz('created_at')->nullable();

            $table->foreign('email_klant')->references('email')->on('users');
            $table->foreign('naam_restaurant')->references('naam')->on('restaurant');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::table('bestelling', function (Blueprint $table) {
        $table->dropForeign('bestelling_email_klant_foreign');
        $table->dropForeign('bestelling_naam_restaurant_foreign');
      });
        Schema::dropIfExists('bestelling');
    }
}
