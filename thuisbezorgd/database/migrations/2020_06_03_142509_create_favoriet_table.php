<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFavorietTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('favoriet', function (Blueprint $table) {
            $table->id();
            $table->string('email_klant');
            $table->foreign('email_klant')->references('email')->on('users');
            $table->string('naam');
            $table->foreign('naam')->references('naam')->on('restaurant');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      // Schema::table('favoriet', function (Blueprint $table) {
      //   $table->dropForeign(['email_klant']);
      //   $table->dropForeign(['naam']);
      // });

        Schema::dropIfExists('favoriet');
    }
}
