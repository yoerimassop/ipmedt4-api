<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product', function (Blueprint $table) {
          $table->id('id');
          $table->string("naam");
          $table->decimal("prijs");
          $table->longText("beschrijving");
          $table->longText("foto");
          $table->string("categorie");
          $table->string('restaurant');
          $table->dateTimeTz('updated_at')->nullable();
          $table->dateTimeTz('created_at')->nullable();

          $table->foreign("restaurant")->references("naam")->on("restaurant");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::table('product', function (Blueprint $table) {
        $table->dropForeign(['restaurant']);
      });
        Schema::dropIfExists('product');
    }
}
