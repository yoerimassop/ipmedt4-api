<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductInBestellingTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product_in_bestelling', function (Blueprint $table) {
            $table->id('id');
            $table->unsignedBigInteger('ordernummer');
            $table->foreignId('product_id');
            $table->integer('aantal');
            //Voor bestelling status update
            $table->dateTimeTz('updated_at')->nullable();
            $table->dateTimeTz('created_at')->nullable();
            $table->foreign('ordernummer')->references('ordernummer')->on('bestelling');
            $table->foreign('product_id')->references('id')->on('product');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product_in_bestelling');
    }
}
