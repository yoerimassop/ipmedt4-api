<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateReviewTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('review', function (Blueprint $table) {
          $table->id();
          $table->string('naam_klant')->default('anoniem');
          $table->string('naam_restaurant');
          $table->double('rating');
          $table->string('review');
          $table->foreign('naam_restaurant')->references('naam')->on('restaurant');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

       Schema::table('review', function (Blueprint $table) {
         $table->dropForeign(['naam_restaurant']);
         $table->dropColumn('review');
       });

        Schema::dropIfExists('review');
    }
}
