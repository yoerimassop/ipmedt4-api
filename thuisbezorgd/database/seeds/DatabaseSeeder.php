<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(user_seeder::class);
        $this->call(restaurant_seeder::class);
        $this->call(bestelling_seeder::class);
        $this->call(product__seeder::class);
        $this->call(product_in_bestelling_seeder::class);
        $this->call(favorieten_seeder::class);
        $this->call(review_seeder::class);
    }
}
