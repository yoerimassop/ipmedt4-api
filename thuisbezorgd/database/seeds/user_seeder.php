<?php

use Illuminate\Database\Seeder;

class user_seeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('users')->insert([
      'naam' =>  'anoniem',
      'email' => 'anoniem',
      'telefoonnummer' => '0637203414',
      'password' => bcrypt('anoniem'),
      'geslacht' => 'm',
      'postcode' => '2215MB',
      'huisnummer' => '13',
      'woonplaats' => 'voorhout',
      'straatnaam' => 'pieterwoutersstraat',
      'geboortedatum' => '2001-02-02',
      'rol' => 'klant'
      ]);

      DB::table('users')->insert([
      'naam' =>  'yoeri massop',
      'email' => 'klant@gmail.com',
      'telefoonnummer' => '0637203414',
      'password' => bcrypt('klant'),
      'geslacht' => 'm',
      'postcode' => '2215MB',
      'huisnummer' => '13',
      'woonplaats' => 'voorhout',
      'straatnaam' => 'pieterwoutersstraat',
      'geboortedatum' => '2001-02-02',
      'rol' => 'klant'
      ]);

      DB::table('users')->insert([
      'naam' =>  'julian visschedijk',
      'email' => 'bezorger@gmail.com',
      'telefoonnummer' => '0612345678',
      'password' => bcrypt('bezorger'),
      'geslacht' => 'm',
      'postcode' => '2211ZV',
      'huisnummer' => '11',
      'woonplaats' => 'Noordwijkerhout',
      'straatnaam' => 'Centurio',
      'geboortedatum' => '2000-01-01',
      'rol' => 'bezorger',
      'location' => 'amsterdam'
      ]);

      DB::table('users')->insert([
      'naam' =>  'Wajos Icarus',
      'email' => 'mykonos@gmail.com',
      'telefoonnummer' => '0612345678',
      'password' => bcrypt('mykonos'),
      'geslacht' => 'm',
      'postcode' => '2161JP',
      'huisnummer' => '90',
      'woonplaats' => 'Lisse',
      'straatnaam' => 'Kanaalstraat',
      'geboortedatum' => '1960-01-01',
      'rol' => 'restaurant'
      ]);

      DB::table('users')->insert([
      'naam' =>  'Kees Baklava',
      'email' => 'donermeetingpoint@gmail.com',
      'telefoonnummer' => '0612345678',
      'password' => bcrypt('donermeetingpoint'),
      'geslacht' => 'm',
      'postcode' => '2161ZR',
      'huisnummer' => '6',
      'woonplaats' => 'Lisse',
      'straatnaam' => 'Meer en Houtplein',
      'geboortedatum' => '1990-01-01',
      'rol' => 'restaurant'
      ]);

      DB::table('users')->insert([
      'naam' =>  'Graaf Jan',
      'email' => 'graafjan@gmail.com',
      'telefoonnummer' => '0612345678',
      'password' => bcrypt('graafjan'),
      'geslacht' => 'm',
      'postcode' => '2171HC',
      'huisnummer' => '3',
      'woonplaats' => 'Sassenheim',
      'straatnaam' => 'Jan van Brabantweg',
      'geboortedatum' => '1990-01-01',
      'rol' => 'restaurant'
      ]);

      DB::table('users')->insert([
      'naam' =>  'Samwayo Fathsuba',
      'email' => 'subwayfresh@hotmail.com',
      'telefoonnummer' => '0612345678',
      'password' => bcrypt('samir'),
      'geslacht' => 'm',
      'postcode' => '2211ZV',
      'huisnummer' => '11',
      'woonplaats' => 'Noordwijkerhout',
      'straatnaam' => 'Centurio',
      'geboortedatum' => '2000-01-01',
      'rol' => 'ondernemer'
      ]);

      DB::table('users')->insert([
      'naam' =>  'Kevin pelt',
      'email' => 'cheers@gmail.com',
      'telefoonnummer' => '0612345678',
      'password' => bcrypt('cheers'),
      'geslacht' => 'm',
      'postcode' => '2211ZV',
      'huisnummer' => '11',
      'woonplaats' => 'Noordwijkerhout',
      'straatnaam' => 'Centurio',
      'geboortedatum' => '2000-01-01',
      'rol' => 'restaurant'
      ]);

      DB::table('users')->insert([
      'naam' =>  'Dick verhaag',
      'email' => 'welgelegen@gmail.com',
      'telefoonnummer' => '0612345678',
      'password' => bcrypt('welgelegen'),
      'geslacht' => 'm',
      'postcode' => '2211ZV',
      'huisnummer' => '11',
      'woonplaats' => 'Noordwijkerhout',
      'straatnaam' => 'Centurio',
      'geboortedatum' => '2000-01-01',
      'rol' => 'restaurant'
      ]);

      DB::table('users')->insert([
      'naam' =>  'Youseff verdutti',
      'email' => 'santamaria@gmail.com',
      'telefoonnummer' => '0612345678',
      'password' => bcrypt('santamaria'),
      'geslacht' => 'm',
      'postcode' => '2211ZV',
      'huisnummer' => '11',
      'woonplaats' => 'Noordwijkerhout',
      'straatnaam' => 'Centurio',
      'geboortedatum' => '2000-01-01',
      'rol' => 'restaurant'
      ]);

      DB::table('users')->insert([
      'naam' =>  'Tarek ait houssie',
      'email' => 'opeigenwijze@gmail.com',
      'telefoonnummer' => '0612345678',
      'password' => bcrypt('opeigenwijze'),
      'geslacht' => 'm',
      'postcode' => '2211ZV',
      'huisnummer' => '11',
      'woonplaats' => 'Noordwijkerhout',
      'straatnaam' => 'Centurio',
      'geboortedatum' => '2000-01-01',
      'rol' => 'restaurant'
      ]);
    }
}
