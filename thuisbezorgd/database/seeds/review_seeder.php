<?php

use Illuminate\Database\Seeder;

class review_seeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('review')->insert([
            'naam_klant' => 'Peter',
            'naam_restaurant' => 'mykonos',
            'rating' => 5,
            'review' => 'lekker gegeten'
          ]);
        DB::table('review')->insert([
            'naam_klant' => 'Jeroen',
            'naam_restaurant' => 'döner meeting point',
            'rating' => 4,
            'review' => 'lekker gegeten en goede bezorging'
          ]);
    }
}
