<?php

use Illuminate\Database\Seeder;
class bestelling_seeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('bestelling')->insert([
        'ordernummer' => 1,
        'totaalbedrag' => 25,
        'bezorgdatum' => "2020-07-01",
        'bezorgtijd' => "13:45",
        'betaalmethode' => "ideal",
        'email_klant' => "klant@gmail.com",
        'email_bezorger' => "bezorger@gmail.com",
        'naam_restaurant' => "mykonos",
        'status' => "bezorgd",
        'bezorgadres' => "Kerstraat 73 Noordwijkerhout"
      ]);

      DB::table('bestelling')->insert([
        'totaalbedrag' => 25,
        'bezorgdatum' => "2020-06-30",
        'bezorgtijd' => "18:30",
        'betaalmethode' => "ideal",
        'email_klant' => "klant@gmail.com",
        'email_bezorger' => "bezorger@gmail.com",
        'naam_restaurant' => "mykonos",
        'status' => "bezorgd",
        'bezorgadres' => "Kerstraat 73 Noordwijkerhout"
      ]);

      DB::table('bestelling')->insert([
        'totaalbedrag' => 25,
        'bezorgdatum' => "2020-04-06",
        'bezorgtijd' => "z.s.m.",
        'betaalmethode' => "ideal",
        'email_klant' => "klant@gmail.com",
        'naam_restaurant' => "döner meeting point",
        'status' => "gemaakt",
        'bezorgadres' => "Kerstraat 73 Noordwijkerhout"
      ]);

      DB::table('bestelling')->insert([
        'totaalbedrag' => 25,
        'bezorgdatum' => "2020-04-06",
        'bezorgtijd' => "18:30",
        'betaalmethode' => "ideal",
        'email_klant' => "klant@gmail.com",
        'email_bezorger' => "bezorger@gmail.com",
        'naam_restaurant' => "mykonos",
        'status' => "geclaimd",
        'bezorgadres' => "Kerstraat 73 Noordwijkerhout"
      ]);

      DB::table('bestelling')->insert([
        'totaalbedrag' => 25,
        'bezorgdatum' => "2020-04-06",
        'bezorgtijd' => "18:30",
        'betaalmethode' => "ideal",
        'email_klant' => "klant@gmail.com",
        'naam_restaurant' => "mykonos",
        'status' => "gemaakt",
        'bezorgadres' => "Kerstraat 73 Noordwijkerhout"
      ]);

      DB::table('bestelling')->insert([
        'totaalbedrag' => 25,
        'bezorgdatum' => "2020-06-23",
        'bezorgtijd' => "18:30",
        'betaalmethode' => "ideal",
        'email_klant' => "klant@gmail.com",
        'naam_restaurant' => "mykonos",
        'status' => "gemaakt",
        'bezorgadres' => "Kerstraat 73 Noordwijkerhout"
      ]);

      DB::table('bestelling')->insert([
        'totaalbedrag' => 25,
        'bezorgdatum' => "2020-06-16",
        'bezorgtijd' => "18:30",
        'betaalmethode' => "ideal",
        'email_klant' => "klant@gmail.com",
        'naam_restaurant' => "mykonos",
        'status' => "geplaatst",
        'bezorgadres' => "Kerstraat 73 Noordwijkerhout"
      ]);

      DB::table('bestelling')->insert([
        'totaalbedrag' => 25,
        'bezorgdatum' => "2020-06-16",
        'bezorgtijd' => "18:30",
        'betaalmethode' => "ideal",
        'email_klant' => "klant@gmail.com",
        'naam_restaurant' => "mykonos",
        'status' => "geplaatst",
        'bezorgadres' => "Kerstraat 73 Noordwijkerhout"
      ]);

      DB::table('bestelling')->insert([
        'totaalbedrag' => 25,
        'bezorgdatum' => "2020-06-16",
        'bezorgtijd' => "18:30",
        'betaalmethode' => "ideal",
        'email_klant' => "klant@gmail.com",
        'naam_restaurant' => "mykonos",
        'status' => "geplaatst",
        'bezorgadres' => "Kerstraat 73 Noordwijkerhout"
      ]);

      DB::table('bestelling')->insert([
        'totaalbedrag' => 25,
        'bezorgdatum' => "2020-06-16",
        'bezorgtijd' => "18:30",
        'betaalmethode' => "ideal",
        'email_klant' => "klant@gmail.com",
        'naam_restaurant' => "mykonos",
        'status' => "geplaatst",
        'bezorgadres' => "Kerstraat 73 Noordwijkerhout"
      ]);
    }
}
