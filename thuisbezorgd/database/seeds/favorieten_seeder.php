<?php

use Illuminate\Database\Seeder;


class favorieten_seeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        DB::table('favoriet')->insert([
          'email_klant' => 'klant@gmail.com',
          'naam' => 'mykonos',
        ]);
    }
}
