<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Review extends Model
{
    protected $table = "review";

    public $timestamps = false;

    protected $fillable = [
        'naam_klant', 'naam_restaurant','rating', 'review'
    ];
}
