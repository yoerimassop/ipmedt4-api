<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Bestelling extends Model
{
  protected $table = "bestelling";

  public function getProducten(){
    return $this->hasMany('App\ProductInBestelling','ordernummer','ordernummer');
  }

  public function getRestaurant(){
    return $this->hasOne('App\Restaurant','naam','naam_restaurant');
  }

  public function getKlant(){
    return $this->hasOne('App\User','email','email_klant');
  }
}
