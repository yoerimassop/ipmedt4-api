<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\ProductInBestelling;
use App\User;

class ProductInBestellingController extends Controller
{
    public function create(Request $request){
        $ProductInBestelling = new ProductInBestelling();
        $ProductInBestelling ->ordernummer = $request->get('ordernummer');
        $ProductInBestelling ->product_id = $request->get('id');
        $ProductInBestelling ->aantal = $request->get('aantal');
        $ProductInBestelling->save();

        return $ProductInBestelling;
    }
}
