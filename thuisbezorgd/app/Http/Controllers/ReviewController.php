<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Review;
use Illuminate\Support\Facades\Validator;


class ReviewController extends Controller
{
  public function show($restaurant){
    return Review::where('naam_restaurant','=',$restaurant)->get();
  }

  public function index(){
    return Review::all();
  }

  public function storeReview(Request $request){

    $review = Review::create([
      'naam_klant' => $request->json()->get('naam_klant'),
      'naam_restaurant' => $request->json()->get('naam_restaurant'),
      'rating' => $request->json()->get('rating'),
      'review' => $request->json()->get('review')
    ]);


    return response()->json(201);
  }

  public function store(Request $request){
    $user = Auth::user();
    $review = new review;
    $review->email_klant = $user->email;
    $review->naam_restaurant = $request->input('naam_restaurant');
    $review->rating = $request->input('rating');
    $review->review = $request->input('review');

    try{
      $review->save();
      return redirect('/review');
    }

    catch(Exception $e){
      return redirect('/review');
    }
  }
}
