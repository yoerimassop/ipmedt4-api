<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use DB;
use App\Restaurant;
use Auth;

class RestaurantController extends Controller
{
  public function show($naam){
    return Restaurant::where('naam','=',$naam)->first();
  }

  public function emailShow($email){
    return Restaurant::where('priveEmail','=',$email)->first();
  }

  public function index(){
    return Restaurant::all();
  }

  public function restaurantKlantbestelling(){
    return Restaurant::select('naam', 'logo', 'rating', 'beschrijving', 'categorie', 'postcode')
    ->orderBy('naam', 'DESC')
    ->get();
  }

  public function info($naam){
    return Restaurant::where('naam','=',$naam)
    ->select('naam', 'logo', 'rating', 'beschrijving', 'categorie')
    ->get();
  }

  // Functie voor retourneren van restaurant met de opgegeven categorie
  public function categorie($categorie){
    if($categorie == "Zie Alle"){
      return Restaurant::select('naam', 'logo', 'rating', 'beschrijving', 'categorie', 'postcode')
      ->orderBy('naam', 'DESC')
      ->get();
    }
    else{
      return
      Restaurant::where('categorie', '=', $categorie)
      ->select('naam', 'logo', 'rating', 'beschrijving', 'categorie', 'postcode')
      ->orderBy('naam', 'DESC')
      ->get();
    }
  }

  public function register(Request $request){
    //unique bij mail
    $validator = Validator::make($request->json()->all(), [
      'naam' => 'required|string|max:255',
      'priveEmail' => 'required|string|email|max:255|',
      'logo' => 'required|string',
      'telefoonnummer' => 'required|string|min:6',
      'KVK_nummer' => 'required|string|min:5',
      'BTW_nummer' => 'required|string|min:5',
      'prive_nummer' => 'required|string|min:6',
      'postcode' => 'required|string|max:7',
      'huisnummer' => 'required|string|max:5',
      'woonplaats' => 'required|string|max:255',
      'straatnaam' => 'required|string|max:255',
      'beschrijving' => 'required|string|max:75',
      'categorie' => 'required|string|max:25',
    ]);







    if($validator->fails()){
      return response()->json($validator->errors()->toJson(),400);
    }

    $restaurant = Restaurant::create([
      'naam' => $request->json()->get('naam'),
      'priveEmail' => $request->json()->get('priveEmail'),
      'logo' => $request->json()->get('logo'),
      'telefoonnummer' => $request->json()->get('telefoonnummer'),
      'KVK_nummer' => $request->json()->get('KVK_nummer'),
      'BTW_nummer' => $request->json()->get('BTW_nummer'),
      'prive_nummer' => $request->json()->get('prive_nummer'),
      'postcode' => $request->json()->get('postcode'),
      'huisnummer' => $request->json()->get('huisnummer'),
      'woonplaats' => $request->json()->get('woonplaats'),
      'straatnaam' => $request->json()->get('straatnaam'),
      'beschrijving' => $request->json()->get('beschrijving'),
      'categorie' => $request->json()->get('categorie')

    ]);


}



}
