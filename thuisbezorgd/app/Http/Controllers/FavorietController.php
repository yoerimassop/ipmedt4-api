<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Favoriet;

class FavorietController extends Controller
{
  public function show($id){
    return Favoriet::where('id','=',$id)->first();
  }

  public function index(){
    return Favoriet::all();
  }
}
