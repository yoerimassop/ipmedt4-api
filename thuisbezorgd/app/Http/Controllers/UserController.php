<?php

namespace App\Http\Controllers;

use DB;
use App\User;
use App\Restaurant;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;

use Tymon\JWTAuth\Facades\JWTAuth;
use Tymon\JWTAuth\Facades\JWTFactory;
use Tymon\JWTAuth\Exceptions\JWTException;
use Tymon\JWTAuth\Exceptions\JWTSubject;
use Tymon\JWTAuth\PayloadFactory;
use Tymon\JWTAuth\JWTManager as JWT;

class UserController extends Controller
{
  public function index(){
    return user::all();
  }

  public function show($email){
    return User::where('email','=',$email)->first();
  }

  public function register(Request $request){
    $validator = Validator::make($request->json()->all(), [
      'naam' => 'required|string|max:255',
      'email' => 'required|string|email|max:255|unique:users',
      'telefoonnummer' => 'required|string|max:13',
      'password' => 'required|string|min:6',
      'geslacht' => 'required|string|max:9',
      'postcode' => 'required|string|max:7',
      'huisnummer' => 'required|string|max:4',
      'woonplaats' => 'required|string|max:255',
      'straatnaam' => 'required|string|max:255',
      'rol'=> 'string|max:20'
    ]);

    if($validator->fails()){
      return response()->json($validator->errors()->toJson(),400);
    }

    $user = User::create([
      'naam' => $request->json()->get('naam'),
      'email' => $request->json()->get('email'),
      'telefoonnummer' => $request->json()->get('telefoonnummer'),
      'password' => Hash::make($request->json()->get('password')),
      'geslacht' => $request->json()->get('geslacht'),
      'postcode' => $request->json()->get('postcode'),
      'huisnummer' => $request->json()->get('huisnummer'),
      'woonplaats' => $request->json()->get('woonplaats'),
      'straatnaam' => $request->json()->get('straatnaam'),
      'rol' => $request->json()->get('rol')
    ]);

    $token = JWTAuth::fromUser($user);

    return response()->json(compact('user','token'), 201);
  }

  public function login(Request $request){

    $credentials = $request->json()->all();

    try {
      if(!$token = JWTAuth::attempt($credentials)){
        return response()->json(['error' => 'invalid_credentials'], 400);
      }
    } catch (JWTException $e) {
      return response()->json(['error' => 'could_not_create_token'],500);
    }

    return response()->json(compact('token'));
  }

  public function getAuthenticatedUser()
   {
       try {
           if (!$user = JWTAuth::parseToken()->authenticate()) {
               return response()->json(['user_not_found'], 404);
           }
       } catch (Tymon\JWTAuth\Exceptions\TokenExpiredException $e) {
           return response()->json(['token_expired'], $e->getStatusCode());
       } catch (Tymon\JWTAuth\Exceptions\TokenInvalidException $e) {
           return response()->json(['token_invalid'], $e->getStatusCode());
       } catch (Tymon\JWTAuth\Exceptions\JWTException $e) {
           return response()->json(['token_absent'], $e->getStatusCode());
       }


        $user->kleur=$user->email;
       return response()->json(compact('user'));

   }

   public function update($email, Request $request){
   // try {
     $user = User::where('email','=',$email)->update(
     [
       "naam" => $request->naam,
       "telefoonnummer" => $request->telefoonnummer,
       "email" =>$request->email,
       "geslacht" => $request->geslacht,
       "postcode" => $request->postcode,
       "huisnummer" => $request->huisnummer,
       "woonplaats" => $request->woonplaats,
       "straatnaam" => $request->straatnaam,
     ]);

     $restaurant = Restaurant::where('priveEmail','=',$email)->update([
       "priveEmail" => $request->email
     ]);

     // $user->save();
     // $restaurant->save();
}

  public function updateBezorgerLocation($email, $location){
    User::where('email','=',$email)->update(array('location' => $location));
  }


}
