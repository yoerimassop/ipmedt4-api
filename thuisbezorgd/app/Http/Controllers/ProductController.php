<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Product;

class ProductController extends Controller
{
  public function show($id){
    return Product::where('id','=',$id)->first();
  }

  public function emailShow($restaurant){
    return Product::where('restaurant','=',$restaurant)->get();
  }

  public function index(){
    return Product::all();
  }

  public function store(Request $request){
    $product = new Product();

    $product->naam = $request->get('naam');
    $product->prijs = $request->get('prijs');
    $product->beschrijving = $request->get('beschrijving');
    $product->foto = $request->get('foto');
    $product->categorie = $request->get('categorie');
    $product->restaurant = $request->get('restaurant');
    $product->updated_at = $request->get('updated_at');
    $product->created_at = $request->get('created_at');

    $product->save();

    return $product;

  }

  public function update($id, Request $request){
      $product = Product::where('id','=',$id)->update([
        "naam" => $request->naam,
        "prijs" => $request->prijs,
        "beschrijving" => $request->beschrijving,
        "foto" => $request->foto,
        "categorie" => $request->categorie,
        "restaurant" => $request->restaurant,
        "updated_at" => $request->updated_at,
        "created_at" => $request->created_at,
      ]);
  }

  public function destroy($id){
    $product = Product::where('id','=',$id)->delete();
    return response()->json('Gerecht verwijderd');
  }
}
