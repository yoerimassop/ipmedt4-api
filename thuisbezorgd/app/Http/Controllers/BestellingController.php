<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Bestelling;
use App\User;

class BestellingController extends Controller
{
  public function show($ordernummer){
    return Bestelling::where('ordernummer','=',$ordernummer)->first();
  }

  public function index(){
    return Bestelling::all();
  }

  public function producten($ordernummer){
    return Bestelling::where('ordernummer','=',$ordernummer)->first()->getProducten;
  }

  public function status($status){
    return Bestelling::where('status','=',$status)->get();
  }

  public function restaurantBestellingen($restaurant){
    return Bestelling::where('naam_restaurant','=',$restaurant)->get();
  }

  public function bezorger($status, $email){
    return Bestelling::where('status','=', $status)->where('email_bezorger', '=', $email)->get();
  }

  public function bezorgdatum($status, $email, $bezorgdatum){
    return Bestelling::where('status','=', $status)->where('email_bezorger', '=', $email)->where('bezorgdatum', '>=', $bezorgdatum)->get();
  }

  public function create(Request $request){
    $bestelling = new Bestelling();
    $bestelling ->naam_restaurant = $request->get('naam_restaurant');
    $bestelling ->email_klant = $request->get('email_klant');
    $bestelling ->totaalbedrag = $request->get('totaalbedrag');
    $bestelling ->bezorgdatum = $request->get('bezorgdatum');
    $bestelling ->bezorgadres = $request->get('bezorgadres');
    $bestelling ->bezorgtijd = $request->get('bezorgtijd');
    $bestelling->save();

    return $bestelling;
  }

  public function laatsteBestelling(){
    return Bestelling::latest()->first();
  }



  public function updateStatus($ordernummer, $status, $email){
    $bestelling = Bestelling::where('ordernummer','=',$ordernummer)->update([
      "status" => $status,
      "email_bezorger" => $email,
    ]);
  }

  public function updateStatusBestelling($ordernummer, Request $request){
    $bestelling = Bestelling::where('ordernummer','=',$ordernummer)->update([
      "totaalbedrag" => $request->totaalbedrag,
      "bezorgdatum" => $request->bezorgdatum,
      "bezorgtijd" => $request->bezorgtijd,
      "betaalmethode" => $request->betaalmethode,
      "email_klant" => $request->email_klant,
      "email_bezorger" => $request->email_bezorger,
      "naam_restaurant" => $request->naam_restaurant,
      "status" => $request->status,
      "bezorgadres" => $request->bezorgadres,
    ]);
  }
}
